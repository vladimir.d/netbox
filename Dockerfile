FROM ubuntu:focal

# default shell
SHELL ["/bin/bash", "-c"]

# Set terminal to be noninteractive
ENV DEBIAN_FRONTEND noninteractive

# Download the latest stable release from GitHub
# https://github.com/netbox-community/netbox/releases
# v3.3.1 - 2022-08-25
ENV NETBOX_VERSION "3.3.1"
ENV NETBOX_PACKAGE "v${NETBOX_VERSION}.tar.gz"


# Damned, infernal sources time/bandwidth wastage!
RUN sed -i 's/deb-src/# deb-src/' /etc/apt/sources.list

# Put main packages in place
RUN apt-get update && \
    apt-get upgrade -y -f && \
    apt-get install --no-install-recommends --allow-unauthenticated -y -f \
        sudo \
        curl \
		wget \
		vim \
		gnupg \
		supervisor \
		net-tools \
		ca-certificates


RUN apt install -y python3 python3-pip python3-venv python3-dev build-essential libxml2-dev libxslt1-dev libffi-dev libpq-dev libssl-dev zlib1g-dev \
	libldap2-dev libsasl2-dev libssl-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /app/code /app/pkg /app/data
WORKDIR /app/code

# Download the latest stable release from GitHub
RUN wget https://github.com/netbox-community/netbox/archive/${NETBOX_PACKAGE} && \
	tar -xzf ${NETBOX_PACKAGE} -C /app/pkg && \
	rm ${NETBOX_PACKAGE} && \
	cp -r /app/pkg/netbox-${NETBOX_VERSION} /app/data/netbox && \
	ln -s /app/data/netbox /app/code/netbox-${NETBOX_VERSION} && \
	ln -s /app/data/netbox /opt/netbox
#	ln -s /app/code/netbox-${NETBOX_VERSION}/ /opt/netbox

# use www-data as the NetBox System User
# assign this user ownership of the media directory
RUN mkdir -p /app/data/netbox && \
	rm -rf /app/code/netbox-${NETBOX_VERSION}/netbox/media && \
	ln -s /app/data/netbox/media /app/code/netbox-${NETBOX_VERSION}/netbox/media && \
	ln -s /app/data/netbox/venv /app/code/netbox-${NETBOX_VERSION}/venv

# LDAP
# https://docs.netbox.dev/en/stable/installation/6-ldap/
#RUN pip install --upgrade virtualenv && \
#	virtualenv -p python3 venv
#RUN source /opt/netbox/venv/bin/activate && \
#	pip3 install django-auth-ldap && \
#	sh -c "echo 'django-auth-ldap' >> /opt/netbox/local_requirements.txt" && \
#	ln -s /app/data/netbox/ldap_config.py /opt/netbox/netbox/netbox/ldap_config.py

# copy configs
# RUN cp /app/code/netbox-${NETBOX_VERSION}/netbox/netbox/configuration_example.py /app/data/netbox/configuration.py
# see further config update in start.sh
RUN ln -s /app/data/netbox/configuration.py /app/code/netbox-${NETBOX_VERSION}/netbox/netbox/configuration.py

# gunicorn configs
RUN ln -s /app/data/netbox/gunicorn.py /opt/netbox/gunicorn.py

# supervisor
ADD config/supervisor/ /app/code/supervisor/
RUN rm -rf /etc/supervisor/conf.d/ && \
	mkdir -p /app/data/supervisor && \
	ln -sf /app/data/supervisor /etc/supervisor/conf.d && \
	ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

RUN rm -rf /var/tmp && \
	ln -s /run /var/tmp

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
