#!/bin/bash

export NETBOX_VERSION=3.3.1
NETBOX_DIR=/app/data/netbox

if [[ ! -d /app/data/default ]]
then

    echo "Initializing..."

    # copy supervisord configs
    cp -R /app/code/supervisor /app/data/supervisor

    # create all necessary directories and configs
#    mkdir -p /app/data/netbox

    cp -R /app/pkg/netbox-${NETBOX_VERSION} ${NETBOX_DIR}


    #
    echo "=> Create Netbox config"
    SECRET_KEY=$(python3 /opt/netbox/netbox/generate_secret_key.py)
    echo "
ALLOWED_HOSTS = ['*']
DATABASE = {
    'NAME': '${CLOUDRON_POSTGRESQL_DATABASE}',         # Database name
    'USER': '${CLOUDRON_POSTGRESQL_USERNAME}',               # PostgreSQL username
    'PASSWORD': '${CLOUDRON_POSTGRESQL_PASSWORD}',           # PostgreSQL password
    'HOST': '${CLOUDRON_POSTGRESQL_HOST}',      # Database server
    'PORT': '${CLOUDRON_POSTGRESQL_PORT}',               # Database port (leave blank for default)
    'CONN_MAX_AGE': 300,      # Max database connection age
}
REDIS = {
    'tasks': {
        'HOST': '${CLOUDRON_REDIS_HOST}',      # Redis server
        'PORT': ${CLOUDRON_REDIS_PORT},             # Redis port
        'PASSWORD': '${CLOUDRON_REDIS_PASSWORD}',           # Redis password (optional)
        'DATABASE': 0,            # Database ID
        'SSL': False,             # Use SSL (optional)
    },
    'caching': {
        'HOST': '${CLOUDRON_REDIS_HOST}',
        'PORT': ${CLOUDRON_REDIS_PORT},
        'PASSWORD': '${CLOUDRON_REDIS_PASSWORD}',
        'DATABASE': 1,            # Unique ID for second database
        'SSL': False,
    }
}
SECRET_KEY = '${SECRET_KEY}'
" > ${NETBOX_DIR}/netbox/netbox/configuration.py

    # Run the Upgrade Script
    # Once NetBox has been configured, we're ready to proceed
    # with the actual installation.
    # We'll run the packaged upgrade script (upgrade.sh) to perform the following actions:
    #
    #   Create a Python virtual environment
    #   Installs all required Python packages
    #   Run database schema migrations
    #   Builds the documentation locally (for offline use)
    #   Aggregate static resource files on disk
    echo " => Netbox installation"
    sudo /opt/netbox/upgrade.sh

    # Create a Super User
    echo " => Create a Super User"
    export DJANGO_SUPERUSER_USERNAME="netboxadmin"
    export DJANGO_SUPERUSER_EMAIL="${CLOUDRON_MAIL_FROM}"
    export DJANGO_SUPERUSER_PASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"

    source /opt/netbox/venv/bin/activate
    cd /opt/netbox/netbox && python3 manage.py createsuperuser --no-input


# LDAP
# https://docs.netbox.dev/en/stable/installation/6-ldap/
#
#source /opt/netbox/venv/bin/activate && \
#	pip3 install django-auth-ldap && \
#	sh -c "echo 'django-auth-ldap' >> /opt/netbox/local_requirements.txt" && \
#	ln -s /app/data/netbox/ldap_config.py /opt/netbox/netbox/netbox/ldap_config.py


    # gunicorn config
    echo " => Gunicorn config"
    cat ${NETBOX_DIR}/contrib/gunicorn.py | \
        sed -e 's/127.0.0.1/0.0.0.0/' \
        > ${NETBOX_DIR}/gunicorn.py


    # create this is first install, so setup /app/data and initial settings
    mkdir -p /app/data/default
fi

echo "Starting NetBox"
# Development server
# Not for production use
# see https://docs.netbox.dev/en/stable/installation/3-netbox/#test-the-application
source /opt/netbox/venv/bin/activate && cd /opt/netbox/netbox && exec python3 manage.py runserver 0.0.0.0:8001 --insecure

# TODO: to run it as WSGI app with supervisord
# https://docs.netbox.dev/en/stable/installation/4-gunicorn/
#exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Netbox

