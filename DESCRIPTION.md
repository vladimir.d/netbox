NetBox is the leading solution for modeling and documenting modern networks. By combining the traditional disciplines of IP address management (IPAM) and datacenter infrastructure management (DCIM) with powerful APIs and extensions, NetBox provides the ideal "source of truth" to power network automation. Read on to discover why thousands of organizations worldwide put NetBox at the heart of their infrastructure.

 Built for Networks

Unlike general-purpose CMDBs, NetBox has curated a data model which caters specifically to the needs of network engineers and operators. It delivers a wide assortment of object types carefully crafted to best serve the needs of infrastructure design and documentation. These cover all facets of network technology, from IP address managements to cabling to overlays and more:

Hierarchical regions, sites, and locations
Racks, devices, and device components
Cables and wireless connections
Power distribution tracking
Data circuits and providers
Virtual machines and clusters
IP prefixes, ranges, and addresses
VRFs and route targets
FHRP groups (VRRP, HSRP, etc.)
AS numbers
VLANs and scoped VLAN groups
L2VPN overlays
Tenancy assignments
Contact management
